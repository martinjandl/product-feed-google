<?php

namespace Nwt\ProductFeed\GoogleBundle;

use Shopsys\Plugin\PluginDataStorageProviderInterface;
use Shopsys\ProductFeed\DomainConfigInterface;
use Shopsys\ProductFeed\FeedConfigInterface;
use Shopsys\ProductFeed\StandardFeedItemInterface;
use Symfony\Component\Translation\TranslatorInterface;

class GoogleFeedConfig implements FeedConfigInterface
{
    /**
     * @var \Nwt\ProductFeed\GoogleBundle\DataStorageProvider
     */
    private $dataStorageProvider;

    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     */
    private $translator;

    /**
     * @var string[]
     */
    private $googleCategoryFullNamesCache = [];

    /**
     * GoogleFeedConfig constructor.
     * @param \Nwt\ProductFeed\GoogleBundle\DataStorageProvider $dataStorageProvider
     * @param \Symfony\Component\Translation\TranslatorInterface $translator
     */
    public function __construct(
        DataStorageProvider $dataStorageProvider,
        TranslatorInterface $translator
    ) {

        $this->translator = $translator;
        $this->dataStorageProvider = $dataStorageProvider;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return 'Google';
    }

    /**
     * @return string
     */
    public function getFeedName()
    {
        return 'google';
    }

    /**
     * @return string
     */
    public function getTemplateFilepath()
    {
        return '@NwtProductFeedGoogle/feed.xml.twig';
    }

    /**
     * @return string
     */
    public function getAdditionalInformation()
    {
        return $this->translator->trans(
            'Google Shopping product feed is not optimized for selling to Australia,
            Czechia, France, Germany, Italy, Netherlands, Spain, Switzerland, the UK,
            and the US. It is caused by missing \'shipping\' atribute.'
        );
    }

    /**
     * @param \Shopsys\ProductFeed\StandardFeedItemInterface[] $items
     * @param \Shopsys\ProductFeed\DomainConfigInterface $domainConfig
     * @return \Shopsys\ProductFeed\StandardFeedItemInterface[]
     */
    public function processItems(array $items, DomainConfigInterface $domainConfig)
    {
        // SELLABLE
        $sellableItems = array_filter($items, [$this, 'isItemSellable']);
        $productsDataById = $this->getProductsDataIndexedByItemId($sellableItems);

        foreach ($sellableItems as $key => $item) {
            // SHOW
            $productId = $item->getId();
            if (!$this->isProductShownOnDomain($productId, $productsDataById, $domainConfig)) {
                unset($sellableItems[$key]);
                continue;
            }

            // CATEGORY
            $categoryName = $this->findGoogleCategoryFullNameByCategoryIdUsingCache($item->getMainCategoryId());
            $item->setCustomValue('category_name', $categoryName);
        }

        return $sellableItems;
    }

    /**
     * @param \Shopsys\ProductFeed\StandardFeedItemInterface[] $items
     * @return array
     */
    private function getProductsDataIndexedByItemId(array $items)
    {
        $productIds = [];
        foreach ($items as $item) {
            $productIds[] = $item->getId();
        }

        $productDataStorage = $this->dataStorageProvider->getProductDataStorage();

        return $productDataStorage->getMultiple($productIds);
    }

    /**
     * @param int $productId
     * @param array $productsDataById
     * @param \Shopsys\ProductFeed\DomainConfigInterface $domainConfig
     * @return bool
     */
    protected function isProductShownOnDomain($productId, $productsDataById, DomainConfigInterface $domainConfig)
    {
        return $productsDataById[$productId]['show'][$domainConfig->getId()] ?? true;
    }

    /**
     * @param \Shopsys\ProductFeed\StandardFeedItemInterface $item
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod) method is used through array_filter
     */
    private function isItemSellable(StandardFeedItemInterface $item)
    {
        return !$item->isSellingDenied();
    }

    /**
     * @param int $categoryId
     * @return string|null
     */
    private function findGoogleCategoryFullNameByCategoryIdUsingCache($categoryId)
    {
        if (!array_key_exists($categoryId, $this->googleCategoryFullNamesCache)) {
            $this->googleCategoryFullNamesCache[$categoryId] = $this->findGoogleCategoryFullNameByCategoryId($categoryId);
        }

        return $this->googleCategoryFullNamesCache[$categoryId];
    }

    /**
     * @param int $categoryId
     * @return string|null
     */
    private function findGoogleCategoryFullNameByCategoryId($categoryId)
    {
        $categoryDataStorage = $this->dataStorageProvider->getCategoryDataStorage();
        $googleCategoryDataStorage = $this->dataStorageProvider->getGoogleCategoryDataStorage();

        $categoryData = $categoryDataStorage->get($categoryId);
        $googleCategoryId = $categoryData[DataStorageProvider::CONTEXT_GOOGLE_CATEGORY] ?? null;

        if ($googleCategoryId !== null) {
            $googleCategoryData = $googleCategoryDataStorage->get($googleCategoryId);

            return $googleCategoryData['full_name'];
        }

        return null;
    }
}
