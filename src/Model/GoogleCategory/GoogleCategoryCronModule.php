<?php

namespace Nwt\ProductFeed\GoogleBundle\Model\GoogleCategory;

use Nwt\ProductFeed\GoogleBundle\DataStorageProvider;
use Shopsys\Plugin\Cron\SimpleCronModuleInterface;
use Symfony\Bridge\Monolog\Logger;

class GoogleCategoryCronModule implements SimpleCronModuleInterface
{
    /**
     * @var \Nwt\ProductFeed\GoogleBundle\Model\GoogleCategory\GoogleCategoryDownloader
     */
    private $googleCategoryDownloader;

    /**
     * @var \Nwt\ProductFeed\GoogleBundle\DataStorageProvider
     */
    private $dataStorageProvider;

    /**
     * @var \Symfony\Bridge\Monolog\Logger
     */
    private $logger;

    /**
     * GoogleCategoryCronModule constructor.
     * @param \Nwt\ProductFeed\GoogleBundle\Model\GoogleCategory\GoogleCategoryDownloader $googleCategoryDownloader
     * @param \Nwt\ProductFeed\GoogleBundle\DataStorageProvider $dataStorageProvider
     */
    public function __construct(
        GoogleCategoryDownloader $googleCategoryDownloader,
        DataStorageProvider $dataStorageProvider
    ) {
        $this->googleCategoryDownloader = $googleCategoryDownloader;
        $this->dataStorageProvider = $dataStorageProvider;
    }

    /**
     * @inheritdoc
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        try {
            $newCategories = $this->googleCategoryDownloader->getGoogleCategories();
            $this->saveGoogleCategories($newCategories);
        } catch (GoogleCategoryDownloadFailedException $e) {
            $this->logger->addError($e->getMessage());
        }
    }

    /**
     * @param array[] $newCategories
     */
    private function saveGoogleCategories(array $newCategories)
    {
        $googleCategoryDataStorage = $this->dataStorageProvider->getGoogleCategoryDataStorage();
        $existingCategoryIds = array_keys($googleCategoryDataStorage->getAll());

        foreach ($newCategories as $id => $category) {
            $googleCategoryDataStorage->set($id, $category);
        }

        $newCategoryIds = array_keys($newCategories);
        $categoryIdsToDelete = array_diff($existingCategoryIds, $newCategoryIds);
        foreach ($categoryIdsToDelete as $categoryIdToDelete) {
            $googleCategoryDataStorage->remove($categoryIdToDelete);
        }

        $this->logger->addInfo(sprintf(
            'Downloaded %d categories (%d old categories deleted).',
            count($newCategoryIds),
            count($categoryIdsToDelete)
        ));
    }
}
