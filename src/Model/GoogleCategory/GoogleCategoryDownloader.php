<?php

namespace Nwt\ProductFeed\GoogleBundle\Model\GoogleCategory;

class GoogleCategoryDownloader
{
    /**
     * @var string
     */
    private $googleCategoryFeedUrl;

    /**
     * @param string $googleCategoryFeedUrl
     */
    public function __construct($googleCategoryFeedUrl)
    {
        $this->googleCategoryFeedUrl = $googleCategoryFeedUrl;
    }

    /**
     * @return array[]
     * @throws \Nwt\ProductFeed\GoogleBundle\Model\GoogleCategory\GoogleCategoryDownloadFailedException
     */
    public function getGoogleCategories()
    {
        return $this->loadData();
    }

    /**
     * @return array
     * @throws \Nwt\ProductFeed\GoogleBundle\Model\GoogleCategory\GoogleCategoryDownloadFailedException
     */
    private function loadData()
    {
        try {
            $data = [];
            $fileData = file($this->googleCategoryFeedUrl, FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
            $version = array_shift($fileData);
            echo "Downloading version: " . $version . PHP_EOL;

            foreach ($fileData as $row) {
                list($id, $categoryFullName) = explode(' - ', $row);
                $catExplode = explode(' > ', $categoryFullName);

                $data[$id] = [
                    'id' => $id,
                    'name' => end($catExplode),
                    'full_name' => $categoryFullName,
                ];
            }

            return $data;
        } catch (\Exception $e) {
            throw new GoogleCategoryDownloadFailedException($e);
        }
    }
}
