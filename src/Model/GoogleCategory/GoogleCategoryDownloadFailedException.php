<?php

namespace Nwt\ProductFeed\GoogleBundle\Model\GoogleCategory;

use Exception;

class GoogleCategoryDownloadFailedException extends Exception
{
    /**
     * @param \Exception $causedBy
     */
    public function __construct(Exception $causedBy)
    {
        $message = sprintf('Download of Google categories failed: "%s"', $causedBy->getMessage());

        parent::__construct($message, $causedBy->getCode(), $causedBy);
    }
}
