<?php

namespace Nwt\ProductFeed\GoogleBundle\Model\GoogleCategory;

use Nwt\ProductFeed\GoogleBundle\DataStorageProvider;
use Shopsys\Plugin\PluginDataFixtureInterface;

class GoogleCategoryDataFixture implements PluginDataFixtureInterface
{
    const GOOGLE_CATEGORY_ID_FIRST = 1;
    const GOOGLE_CATEGORY_ID_SECOND = 2;
    const GOOGLE_CATEGORY_ID_THIRD = 3;

    /**
     * @var \Nwt\ProductFeed\GoogleBundle\DataStorageProvider
     */
    private $dataStorageProvider;

    /**
     * GoogleCategoryDataFixture constructor.
     * @param \Nwt\ProductFeed\GoogleBundle\DataStorageProvider $pluginDataStorageProvider
     */
    public function __construct(DataStorageProvider $pluginDataStorageProvider)
    {
        $this->dataStorageProvider = $pluginDataStorageProvider;
    }

    /**
     * load
     */
    public function load()
    {
        $dataStorage = $this->getGoogleCategoryDataStorage();
        $dataFixture = $this->getData();
        foreach ($dataFixture as $key => $data) {
            $dataStorage->set($key, $data);
        }
    }

    /**
     * @return array
     */
    public function getData()
    {
        $googleCategoryData = [];

        $googleCategoryData[self::GOOGLE_CATEGORY_ID_FIRST] = [
            'id' => self::GOOGLE_CATEGORY_ID_FIRST,
            'name' => 'Vytápění bazénů',
            'full_name' => 'Dům a zahrada > Bazény a lázně > Bazény a lázně – příslušenství > Vytápění bazénů',
        ];
        $googleCategoryData[self::GOOGLE_CATEGORY_ID_SECOND] = [
            'id' => self::GOOGLE_CATEGORY_ID_SECOND,
            'name' => 'Dekorativní samolepky do interiéru',
            'full_name' => 'Dům a zahrada > Bytové doplňky a dekorace > Dekorativní samolepky do interiéru',
        ];
        $googleCategoryData[self::GOOGLE_CATEGORY_ID_THIRD] = [
            'id' => self::GOOGLE_CATEGORY_ID_THIRD,
            'name' => 'Sady na přestavbu dětských postýlek',
            'full_name' => 'Nábytek > Nábytek pro děti a kojence > Dětské postýlky – příslušenství > Sady na přestavbu dětských postýlek',
        ];

        return $googleCategoryData;
    }

    /**
     * @return \Shopsys\Plugin\DataStorageInterface
     */
    private function getGoogleCategoryDataStorage()
    {
        return $this->dataStorageProvider->getGoogleCategoryDataStorage();
    }
}
