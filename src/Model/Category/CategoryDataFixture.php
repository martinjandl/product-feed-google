<?php

namespace Nwt\ProductFeed\GoogleBundle\Model\Category;

use Nwt\ProductFeed\GoogleBundle\DataStorageProvider;
use Nwt\ProductFeed\GoogleBundle\Model\GoogleCategory\GoogleCategoryDataFixture;
use Shopsys\Plugin\PluginDataFixtureInterface;

class CategoryDataFixture implements PluginDataFixtureInterface
{
    // category with ID 1 is usually hidden root category
    const CATEGORY_ID_FIRST = 2;
    const CATEGORY_ID_SECOND = 3;
    const CATEGORY_ID_THIRD = 4;
    const CATEGORY_ID_FOURTH = 5;
    const CATEGORY_ID_FIFTH = 6;

    /**
     * @var \Nwt\ProductFeed\GoogleBundle\DataStorageProvider
     */
    private $dataStorageProvider;

    /**
     * CategoryDataFixture constructor.
     * @param \Nwt\ProductFeed\GoogleBundle\DataStorageProvider $pluginDataStorageProvider
     */
    public function __construct(DataStorageProvider $pluginDataStorageProvider)
    {
        $this->dataStorageProvider = $pluginDataStorageProvider;
    }

    public function load()
    {
        $categoryDataStorage = $this->dataStorageProvider->getCategoryDataStorage();

        $categoryDataStorage->set(self::CATEGORY_ID_FIRST, [
            DataStorageProvider::CONTEXT_GOOGLE_CATEGORY => GoogleCategoryDataFixture::GOOGLE_CATEGORY_ID_FIRST,
        ]);
        $categoryDataStorage->set(self::CATEGORY_ID_SECOND, [
            DataStorageProvider::CONTEXT_GOOGLE_CATEGORY => GoogleCategoryDataFixture::GOOGLE_CATEGORY_ID_SECOND,
        ]);
        $categoryDataStorage->set(self::CATEGORY_ID_THIRD, [
            DataStorageProvider::CONTEXT_GOOGLE_CATEGORY => GoogleCategoryDataFixture::GOOGLE_CATEGORY_ID_SECOND,
        ]);
        $categoryDataStorage->set(self::CATEGORY_ID_FOURTH, [
            DataStorageProvider::CONTEXT_GOOGLE_CATEGORY => GoogleCategoryDataFixture::GOOGLE_CATEGORY_ID_THIRD,
        ]);
        $categoryDataStorage->set(self::CATEGORY_ID_FIFTH, [
            DataStorageProvider::CONTEXT_GOOGLE_CATEGORY => GoogleCategoryDataFixture::GOOGLE_CATEGORY_ID_THIRD,
        ]);
    }
}
