<?php

namespace Nwt\ProductFeed\GoogleBundle\Model\Category;

use Nwt\ProductFeed\GoogleBundle\DataStorageProvider;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;

class CategoryFormType extends AbstractType
{
    /**
     * @var \Symfony\Component\Translation\TranslatorInterface
     */
    private $translator;

    /**
     * @var \Nwt\ProductFeed\GoogleBundle\DataStorageProvider
     */
    private $dataStorageProvider;

    /**
     * CategoryFormType constructor.
     * @param \Symfony\Component\Translation\TranslatorInterface $translator
     * @param \Nwt\ProductFeed\GoogleBundle\DataStorageProvider $dataStorageProvider
     */
    public function __construct(
        TranslatorInterface $translator,
        DataStorageProvider $dataStorageProvider
    ) {
        $this->translator = $translator;
        $this->dataStorageProvider = $dataStorageProvider;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param  array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $allGoogleCategories = $this->dataStorageProvider->getGoogleCategoryDataStorage()->getAll();

        $builder->add(DataStorageProvider::CONTEXT_GOOGLE_CATEGORY, ChoiceType::class, [
            'label' => $this->translator->trans('Google category'),
            'choices' => array_column($allGoogleCategories, 'id', 'name'),
            'required' => false,
            'attr' => ['class' => 'js-autocomplete-selectbox'],
        ]);
    }
}
