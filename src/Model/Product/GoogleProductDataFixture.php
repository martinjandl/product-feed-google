<?php

namespace Nwt\ProductFeed\GoogleBundle\Model\Product;

use Nwt\ProductFeed\GoogleBundle\DataStorageProvider;
use Shopsys\Plugin\PluginDataFixtureInterface;

class GoogleProductDataFixture implements PluginDataFixtureInterface
{
    const DOMAIN_ID_FIRST = 1;
    const DOMAIN_ID_SECOND = 2;
    const PRODUCT_ID_FIRST = 1;
    const PRODUCT_ID_SECOND = 2;
    const PRODUCT_ID_THIRD = 3;
    const PRODUCT_ID_FOURTH = 4;
    const PRODUCT_ID_FIFTH = 5;

    /**
     * @var \Nwt\ProductFeed\GoogleBundle\DataStorageProvider
     */
    private $dataStorageProvider;

    /**
     * GoogleProductDataFixture constructor.
     * @param \Nwt\ProductFeed\GoogleBundle\DataStorageProvider $dataStorageProvider
     */
    public function __construct(DataStorageProvider $dataStorageProvider)
    {
        $this->dataStorageProvider = $dataStorageProvider;
    }

    public function load()
    {
        $productDataStorage = $this->dataStorageProvider->getProductDataStorage();

        $productDataStorage->set(self::PRODUCT_ID_FIRST, [
            'show' => [
                self::DOMAIN_ID_FIRST => true,
                self::DOMAIN_ID_SECOND => true,
            ],
        ]);

        $productDataStorage->set(self::PRODUCT_ID_SECOND, [
            'show' => [
                self::DOMAIN_ID_FIRST => true,
                self::DOMAIN_ID_SECOND => true,
            ],
        ]);

        $productDataStorage->set(self::PRODUCT_ID_THIRD, [
            'show' => [
                self::DOMAIN_ID_FIRST => true,
                self::DOMAIN_ID_SECOND => false,
            ],
        ]);

        $productDataStorage->set(self::PRODUCT_ID_FOURTH, [
            'show' => [
                self::DOMAIN_ID_FIRST => false,
                self::DOMAIN_ID_SECOND => true,
            ],
        ]);

        $productDataStorage->set(self::PRODUCT_ID_FIFTH, [
            'show' => [
                self::DOMAIN_ID_FIRST => false,
                self::DOMAIN_ID_SECOND => false,
            ],
        ]);
    }
}
