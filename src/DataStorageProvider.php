<?php

namespace Nwt\ProductFeed\GoogleBundle;

use Shopsys\Plugin\PluginDataStorageProviderInterface;

class DataStorageProvider
{
    const CONTEXT_PRODUCT = 'product';
    const CONTEXT_CATEGORY = 'category';
    const CONTEXT_GOOGLE_CATEGORY = 'google_category';

    /**
     * @var \Shopsys\Plugin\PluginDataStorageProviderInterface
     */
    private $pluginDataStorageProvider;

    /**
     * @param \Shopsys\Plugin\PluginDataStorageProviderInterface $dataStorageProvider
     */
    public function __construct(PluginDataStorageProviderInterface $dataStorageProvider)
    {
        $this->pluginDataStorageProvider = $dataStorageProvider;
    }

    /**
     * @return \Shopsys\Plugin\DataStorageInterface
     */
    public function getProductDataStorage()
    {
        return $this->pluginDataStorageProvider
            ->getDataStorage(NwtProductFeedGoogleBundle::class, self::CONTEXT_PRODUCT);
    }

    /**
     * @return \Shopsys\Plugin\DataStorageInterface
     */
    public function getCategoryDataStorage()
    {
        return $this->pluginDataStorageProvider
            ->getDataStorage(NwtProductFeedGoogleBundle::class, self::CONTEXT_CATEGORY);
    }

    /**
     * @return \Shopsys\Plugin\DataStorageInterface
     */
    public function getGoogleCategoryDataStorage()
    {
        return $this->pluginDataStorageProvider
            ->getDataStorage(NwtProductFeedGoogleBundle::class, self::CONTEXT_GOOGLE_CATEGORY);
    }
}
