# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.1.6] - 2018-10-24 (NWT)
### Added
- Cron google categories downloader (@martinGrolmus)
- Google category settings in category (Crud extension) (@martinGrolmus)
- Google product settings in product (Crud extension) (@martinGrolmus)
- Category data fixtures (@martinGrolmus)
- Product data fixtures (@martinGrolmus)

### Fixed
- small fixies in code + PHPDocs (@martinGrolmus)

## [0.1.5] - 2018-10-02 (NWT)
### Added
- Google tag (<g:product_type>) (@martinGrolmus)

## [0.1.2] - 2018-02-12 (Shopsys)
### Fixed
- Fix availability value (@simara-svatopluk)

### Added
- [template for github pull requests](docs/PULL_REQUEST_TEMPLATE.md) (@stanoMilan)

## [0.1.1] - 2017-10-04 (Shopsys)
### Added
- support for shopsys/plugin-interface 0.3.0 (@PetrHeinz)
- support for shopsys/product-feed-interface 0.5.0 (@PetrHeinz)

## 0.1.0 - 2017-09-25 (Shopsys)
### Added
- added basic logic of product feed for Google (@MattCzerner)
- composer.json: added shopsys/coding-standards into require-dev (@MattCzerner)

[Unreleased]: https://github.com/shopsys/product-feed-google/compare/v0.1.1...HEAD
[0.1.1]: https://github.com/shopsys/product-feed-google/compare/v0.1.0...v0.1.1
[0.1.2]: https://github.com/shopsys/product-feed-google/compare/v0.1.1...v0.1.2
